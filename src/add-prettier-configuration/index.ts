import { strings, virtualFs, workspaces } from "@angular-devkit/core";
import {
  apply,
  applyTemplates,
  move,
  Rule,
  SchematicContext,
  SchematicsException,
  Tree,
  chain,
  mergeWith,
  url,
  MergeStrategy,
  // externalSchematic,
} from "@angular-devkit/schematics";
import { normalize } from "path";
import { PrettierSchema } from "./schema";

export function createHost(tree: Tree): workspaces.WorkspaceHost {
  return {
    async readFile(path: string): Promise<string> {
      const data = tree.read(path);
      if (!data) {
        throw new SchematicsException("File not found.");
      }
      return virtualFs.fileBufferToString(data);
    },
    async writeFile(path: string, data: string): Promise<void> {
      return tree.overwrite(path, data);
    },
    async isDirectory(path: string): Promise<boolean> {
      return !tree.exists(path) && tree.getDir(path).subfiles.length > 0;
    },
    async isFile(path: string): Promise<boolean> {
      return tree.exists(path);
    },
  };
}

export function addPrettierConfiguration(options: PrettierSchema): Rule {
  return async (_tree: Tree, _context: SchematicContext) => {
    const directory = `./${options.name}/src/`;

    const templateSource = apply(url("./files"), [
      applyTemplates({
        classify: strings.classify,
        dasherize: strings.dasherize,
        name: "laquetecuento",
      }),
      move(normalize(directory)),
    ]);

    return chain([mergeWith(templateSource, MergeStrategy.Overwrite)]);
  };
}
