import {
  chain,
  externalSchematic,
  Rule,
  schematic,
  SchematicContext,
  Tree,
} from "@angular-devkit/schematics";

// You don't have to export the function as default. You can also have more than one rule factory
// per file.
export function newProject(options: any): Rule {
  return async (_tree: Tree, _context: SchematicContext) => {
    const rule = externalSchematic("@schematics/angular", "ng-new", options);
    const rule2 = schematic("add-prettier-configuration", options);

    return chain([rule, rule2]);
  };
}
